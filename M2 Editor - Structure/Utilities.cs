﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
class WoWFile
{
    public WoWFile(string filename)
    {
        readIndex = 0;
        result = ReadAllBytes(filename);
    }
    private readonly byte[] result;
    public static byte[] ReadAllBytes(string filename)
    {
        return File.ReadAllBytes(filename);
    }

    public int readIndex;
    //Reading
    public int ReadInt8()
    {
        readIndex++;
        return (char)result[readIndex - 1];
    }
    public int ReadInt8(int offset)
    {
        return (char)result[offset];
    }
    public int ReadInt16()
    {
        int output = BitConverter.ToInt16(result, readIndex);
        readIndex += 2;
        return output;

    }
    public int ReadInt16(int offset)
    {
        int output = BitConverter.ToInt16(result, offset);
        readIndex += 2;
        return output;
    }
    public int ReadInt32()
    {
        int output = BitConverter.ToInt32(result, readIndex);
        readIndex += 4;
        return output;

    }
    public int ReadInt32(int offset)
    {
        int output = BitConverter.ToInt32(result, offset);

        return output;
    }
    public char ReadChar()
    {
        readIndex++;
        return ((char)result[readIndex - 1] < 128 && (char)result[readIndex - 1] > 32) ? (char)result[readIndex - 1] : '.';
    }
    public char ReadChar(int offset)
    {
        return ((char)result[offset] < 128 && (char)result[offset] > 32) ? (char)result[offset] : '.';
    }
    public float ReadFloat()
    {
        float output = BitConverter.ToSingle(result, readIndex);
        readIndex += 4;
        return output;
    }
    public float ReadFloat(int offset)
    {
        return BitConverter.ToSingle(result, offset);
    }
    public void SetReadIndex(int value)
    {
        readIndex = value;
    }
    public int GetReadIndex()
    {
        return readIndex;
    }
    //Writing ( not yet implemented ) 
};


