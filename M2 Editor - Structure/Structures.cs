﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


enum E_HEAD_OR_TAIL
{
    HT_HEAD = 0,
    HT_TAIL = 1,
    HT_BOTH = 2
}
enum PARTICLE_TYPE
{
    Normal = 0,
    Quads = 1,
    Deeprun = 2
}
enum EMMITER_TYPE
{
    Plane = 1,
    Sphere = 2,
    Spline_not_used = 3
}
enum E_GLOBALFLAGS
{
    GF_NORMAL = 0,
    GF_TILTINGX = 1,
    GF_TILTINGY__ = 2,
    GF_TILTINGXY = 3,
    GF_ADDITIONAL_HEADER_FIELD = 8,
    GF_UNKNONWN = 16
};

class WVector2
{

    public WVector2()
    {
        x = 0.0f;
        y = 0.0f;
    }
    public WVector2(float a, float b)
    {
        x = a;
        y = b;
    }
    public float x;
    public float y;
};
class WVector3i
{
    public int x;
    public int y;
    public int z;
    public WVector3i()
    {
        x = 0;
        y = 0;
        z = 0;
    }
    public WVector3i(int a, int b, int c)
    {
        x = a;
        y = b;
        z = c;
    }
};
class WVector3
{
    public float x;
    public float y;
    public float z;
    public WVector3()
    {
        x = 0;
        y = 0;
        z = 0;
    }
    public WVector3(float a, float b, float c)
    {
        x = a;
        y = b;
        z = c;
    }
};
class WVector4
{
    public WVector4()
    {

    }
    public WVector4(float x, float y, float z, float w)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }
    public float x, y, z, w;
}
class ByteVector4
{
    public int x;
    public int y;
    public int z;
    public int w;
    public ByteVector4()
    {
        x = 0;
        y = 0;
        z = 0;
        w = 0;
    }
    public ByteVector4(int a, int b, int c, int d)
    {
        x = a;
        y = b;
        z = c;
        w = d;
    }
};
class TheFloats
{
    public TheFloats()
    {

    }

    public WVector3 VertexBoxMin;
    public WVector3 VertexBoxMax;
    public float VertexBoxRadius;

    public WVector3 BoundingBoxMax;
    public WVector3 BoundingBoxMin;
    public float BoundingBoxRadius;

};
class SubAnimBlock_S
{
    public SubAnimBlock_S()
    {
        anim = new List<ByteVector4>();
    }
    public int nValues;
    public int ofsValues;
    public List<ByteVector4> anim;
}
class SubAnimBlock_b
{
    public SubAnimBlock_b()
    {
        value = new List<byte>();
    }
    public int nValues;
    public int ofsValues;
    public List<byte> value;
}
class SubAnimBlock_T
{
    public SubAnimBlock_T()
    {
        timestamp = new List<int>();
    }
    public int nValues;
    public int ofsValues;
    public List<int> timestamp;
}
class SubAnimBlock_F
{
    public SubAnimBlock_F()
    {
        substruct = new List<WVector3>();
    }
    public int nValues;
    public int ofsValues;
    public List<WVector3> substruct;
}
class ABlock_F
{
    public ABlock_F()
    {
        Timestamps = new List<SubAnimBlock_T>();
        Values = new List<SubAnimBlock_F>();
    }
    public int Interpolation;
    public int GlobalSequenceID;
    public int numberOfTimestampPairs;
    public int offsetToTimestampPairs;
    public List<SubAnimBlock_T> Timestamps;
    public int numberOfKeyFramePairs;
    public int offsetToKeyFramePairs;
    public List<SubAnimBlock_F> Values;
}
class ABlock_S
{
    public ABlock_S()
    {
        Timestamps = new List<SubAnimBlock_T>();
        Values = new List<SubAnimBlock_S>();
    }
    public int Interpolation;
    public int GlobalSequenceID;
    public int numberOfTimestampPairs;
    public int offsetToTimestampPairs;
    public List<SubAnimBlock_T> Timestamps;
    public int numberOfKeyFramePairs;
    public int offsetToKeyFramePairs;
    public List<SubAnimBlock_S> Values;
}
class Bone
{
    public Bone()
    {

    }
    public int KeyBoneID;
    public int flags;
    public int ParentBone;
    public WVector3i unk;
    public ABlock_F Translation;
    public ABlock_S Rotation;
    public ABlock_F Scaling;
    public WVector3 PivotPoint;
}
class Particle
{
    public Particle()
    {

    }
    public int id;
    public int flag;
    public int flag2;
    WVector3 pos;
    int bone;
    int texture;
    int FileName1_length;
    int FileName1_offset;
    int FileName2_length;
    int FileName2_offset;

    char blend;
    EMMITER_TYPE Emitter_Type;
    int particle_color_Replaceable;
    PARTICLE_TYPE particle_type;
    E_HEAD_OR_TAIL head_or_tail;
    int Textureroot;
    int rows;
    int cols;
    ABlock_F emissionspeed;
    ABlock_F Speed_var;
    ABlock_F Vertical_range;
    ABlock_F Horizontal_range;
    ABlock_F Gravity;
    ABlock_F Lifespan;
    int paddingoO;
    ABlock_F Emissionrate;
    int paddingoO2;
    ABlock_F Emissionarea_length;
    ABlock_F Emissionarea_width;
    ABlock_F Gravity_strong;
    FakeABlock_F Color;
    FakeABlock_S Opacity;
    FakeABlock_FF Scale;
    List<int> unk1;
    FakeABlock_S Intensity;
    FakeABlock_S UnknownFB2;
    WVector3 unk2;
    WVector3 scale;
    float slowdown;
    WVector2 unkfloats1;
    float rotation;
    WVector2 unkfloats2;
    WVector3 Rot1;
    WVector3 Rot2;
    WVector3 Trans;
    List<float> f2;
    ABlock_b en;
}
class ABlock_b
{
    public ABlock_b()
    {
        Timestamps = new List<SubAnimBlock_T>();
        Values = new List<SubAnimBlock_b>();
    }
    public int Interpolation;
    public int GlobalSequenceID;
    public int numberOfTimestampPairs;
    public int offsetToTimestampPairs;
    public List<SubAnimBlock_T> Timestamps;
    public int numberOfKeyFramePairs;
    public int offsetToKeyFramePairs;
    public List<SubAnimBlock_b> Values;
}
class FakeABlock_F
{
    public FakeABlock_F()
    {
        Timestamps = new List<int>();
        Values = new List<WVector2>();
    }
    public int numberOfTimestampPairs;
    public int offsetToTimestampPairs;
    public List<int> Timestamps;
    public int numberOfKeyFramePairs;
    public int offsetToKeyFramePairs;
    public List<WVector2> Values;
}
class FakeABlock_S
{
    public FakeABlock_S()
    {
        Timestamps = new List<int>();
        Values = new List<WVector4>();
    }

    public int numberOfTimestampPairs;
    public int offsetToTimestampPairs;
    public List<int> Timestamps;
    public int numberOfKeyFramePairs;
    public int offsetToKeyFramePairs;
    public List<WVector4> Values;
}
class FakeABlock_FF
{
    public FakeABlock_FF()
    {
        Timestamps = new List<int>();
        Values = new List<WVector3>();
    }

    public int numberOfTimestampPairs;
    public int offsetToTimestampPairs;
    public List<int> Timestamps;
    public int numberOfKeyFramePairs;
    public int offsetToKeyFramePairs;
    public List<WVector3> Values;
}
class Vertex
{
    public Vertex(float x, float y, float z, int bwx, int bwy, int bwz, int bww, int bix, int biy, int biz, int biw, float nx, float ny, float nz, float tx, float ty, float unkx, float unky)
    {
        Position = new WVector3(x, y, z);
        BoneWeight = new ByteVector4(bwx, bwy, bwz, bww);
        BoneIndices = new ByteVector4(bix, biy, biz, biw);
        Normals = new WVector3(nx, ny, nz);
        TextureCoords = new WVector2(tx, ty);
        Unknown = new WVector2(unkx, unky);
    }
    public Vertex()
    {
        Position = new WVector3();
        BoneWeight = new ByteVector4();
        BoneIndices = new ByteVector4();
        Normals = new WVector3();
        TextureCoords = new WVector2();
        Unknown = new WVector2();
    }
    public WVector3 Position;
    public ByteVector4 BoneWeight;
    public ByteVector4 BoneIndices;
    public WVector3 Normals;
    public WVector2 TextureCoords;
    public WVector2 Unknown;

};
class SkinHeader
{

    public SkinHeader()
    {

    }
    public int nIndices;
    public int ofsIndices;
    public int nTriangles;
    public int ofsTriangles;
    public int nProperties;
    public int ofsProperties;
    public int nSubmeshes;
    public int ofsSubmeshes;
    public int nTextureUnits;
    public int ofsTextureUnits;
    public int LOD;
};
class Submesh
{
    public Submesh()
    {

    }
    public int ID;
    public int StartVertex;
    public int nVertices;
    public int StartTriangle;
    public int nTriangles;
    public int nBones;
    public int StartBones;
    public int Unknown;
    public int RootBone;
    public WVector3 Position;
    public WVector4 Floats;
}
class M2Model
{

    public M2Model(string fileName)
    {
        WoWFile file = new WoWFile(fileName);
        MAGIC = new char[4];
        vertices = new List<Vertex>();
        bones = new List<Bone>();
        //MAGIC
        MAGIC[0] = file.ReadChar();
        MAGIC[1] = file.ReadChar();
        MAGIC[2] = file.ReadChar();
        MAGIC[3] = file.ReadChar();
        string test = "MD20";
        Console.WriteLine(MAGIC);
        if (!MAGIC.ToString().Equals(test)) // valid format for M2
        {
            //Continue with the rest
            IVersion = file.ReadInt32();
            IName = file.ReadInt32();
            ofsName = file.ReadInt32();
            type = (E_GLOBALFLAGS)file.ReadInt32();
            nGlobalSequences = file.ReadInt32();
            ofsGlovalSequences = file.ReadInt32();
            nAnimations = file.ReadInt32();
            ofsAnimations = file.ReadInt32();
            nAnimationLookup = file.ReadInt32();
            ofsAnimationLookup = file.ReadInt32();
            nBones = file.ReadInt32();
            ofsBones = file.ReadInt32();
            nKeyBoneLookup = file.ReadInt32();
            ofsKeyBoneLookup = file.ReadInt32();
            nVertices = file.ReadInt32();
            ofsVertices = file.ReadInt32();
            nViews = file.ReadInt32();
            nColors = file.ReadInt32();
            ofsColors = file.ReadInt32();
            nTextures = file.ReadInt32();
            ofsTextures = file.ReadInt32();
            nTransparency = file.ReadInt32();
            ofsTransparency = file.ReadInt32();
            nTextureanimations = file.ReadInt32();
            ofsTextureanimations = file.ReadInt32();
            nTexReplace = file.ReadInt32();
            ofsTexReplace = file.ReadInt32();
            nRenderFlags = file.ReadInt32();
            ofsRenderFlags = file.ReadInt32();
            nBoneLookupTable = file.ReadInt32();
            ofsBoneLookupTable = file.ReadInt32();
            nTexLookup = file.ReadInt32();
            ofsTexLookup = file.ReadInt32();
            nTexUnits = file.ReadInt32();
            ofsTexUnits = file.ReadInt32();
            nTransLookup = file.ReadInt32();
            ofsTransLookup = file.ReadInt32();
            nTexAnimLookup = file.ReadInt32();
            ofsTexAnimLookup = file.ReadInt32();

            //TheFloats - a bit messy
            float x, y, z;
            theFloats = new TheFloats();
            x = file.ReadFloat();
            y = file.ReadFloat();
            z = file.ReadFloat();
            theFloats.VertexBoxMin = new WVector3(x, y, z);
            x = file.ReadFloat();
            y = file.ReadFloat();
            z = file.ReadFloat();
            theFloats.VertexBoxMax = new WVector3(x, y, z);
            theFloats.VertexBoxRadius = file.ReadFloat();
            x = file.ReadFloat();
            y = file.ReadFloat();
            z = file.ReadFloat();
            theFloats.BoundingBoxMin = new WVector3(x, y, z);
            x = file.ReadFloat();
            y = file.ReadFloat();
            z = file.ReadFloat();
            theFloats.BoundingBoxMin = new WVector3(x, y, z);
            theFloats.BoundingBoxRadius = file.ReadFloat();
            // End of the floats
            for (int i = 0; i < IName - 1; i++)
            {
                ModelName += file.ReadChar(ofsName + i);
            }
            //Vertices
            file.SetReadIndex(ofsVertices);
            for (int i = 0; i < nVertices; i++)
            {
                Vertex vertex = new Vertex();

                vertex.Position.x = file.ReadFloat();
                vertex.Position.y = file.ReadFloat();
                vertex.Position.z = file.ReadFloat();

                vertex.BoneWeight.x = file.ReadInt8();
                vertex.BoneWeight.y = file.ReadInt8();
                vertex.BoneWeight.z = file.ReadInt8();
                vertex.BoneWeight.w = file.ReadInt8();

                vertex.BoneIndices.x = file.ReadInt8();
                vertex.BoneIndices.y = file.ReadInt8();
                vertex.BoneIndices.z = file.ReadInt8();
                vertex.BoneIndices.w = file.ReadInt8();

                vertex.Normals.x = file.ReadFloat();
                vertex.Normals.y = file.ReadFloat();
                vertex.Normals.z = file.ReadFloat();

                vertex.TextureCoords.x = file.ReadFloat();
                vertex.TextureCoords.y = file.ReadFloat();

                vertex.Unknown.x = file.ReadFloat();
                vertex.Unknown.y = file.ReadFloat();

                vertices.Add(vertex); // add the final vertex to the list
            }

            //Bones
            int indexHolder = 0;
            file.SetReadIndex(6904);

            Bone bone = new Bone();
            bone.KeyBoneID = file.ReadInt32();
            bone.flags = file.ReadInt32();
            bone.ParentBone = file.ReadInt16();

            WVector3i unkn = new WVector3i();
            unkn.x = file.ReadInt16();
            unkn.y = file.ReadInt16();
            unkn.z = file.ReadInt16();
            bone.unk = unkn;

            //Translation Block
            ABlock_F translation = new ABlock_F();
            translation.Interpolation = file.ReadInt16();
            translation.GlobalSequenceID = file.ReadInt16();
            translation.numberOfTimestampPairs = file.ReadInt32();
            translation.offsetToTimestampPairs = file.ReadInt32();

            indexHolder = file.GetReadIndex();
            file.SetReadIndex(translation.offsetToTimestampPairs);
            //Read TimeStamps
            translation.Timestamps = new List<SubAnimBlock_T>();
            for (int j = 0; j < translation.numberOfTimestampPairs; j++)
            {
                SubAnimBlock_T element = new SubAnimBlock_T();
                element.nValues = file.ReadInt32();
                element.ofsValues = file.ReadInt32();

                for (int k = 0; k < element.nValues; k++)
                {
                    int var = file.ReadInt32(element.ofsValues + 4 * k);
                    element.timestamp.Add(var);
                }
                translation.Timestamps.Add(element);
            }
            file.SetReadIndex(indexHolder); // Continue with the index
            translation.numberOfKeyFramePairs = file.ReadInt32();
            translation.offsetToKeyFramePairs = file.ReadInt32();

            //Read Values
            indexHolder = file.GetReadIndex();
            file.SetReadIndex(translation.offsetToKeyFramePairs);
            translation.Values = new List<SubAnimBlock_F>();
            for (int j = 0; j < translation.numberOfKeyFramePairs; j++)
            {

                SubAnimBlock_F element = new SubAnimBlock_F();
                element.nValues = file.ReadInt32();
                element.ofsValues = file.ReadInt32();

                int tempIndex = file.GetReadIndex();
                file.SetReadIndex(element.ofsValues);

                for (int k = 0; k < element.nValues; k++)
                {
                    WVector3 vector = new WVector3();
                    vector.x = file.ReadFloat();
                    vector.y = file.ReadFloat();
                    vector.z = file.ReadFloat();
                    element.substruct.Add(vector);

                }
                file.SetReadIndex(tempIndex);
                translation.Values.Add(element);
            }
            file.SetReadIndex(indexHolder); // Continue with the next block
            bone.Translation = translation; // Assign the block to the newly created translation structure

            
            //Rotation Block
            ABlock_S rotation = new ABlock_S();
            rotation.Interpolation = file.ReadInt16();
            rotation.GlobalSequenceID = file.ReadInt16();
            rotation.numberOfTimestampPairs = file.ReadInt32();
            rotation.offsetToTimestampPairs = file.ReadInt32();

            indexHolder = file.GetReadIndex();
            file.SetReadIndex(rotation.offsetToTimestampPairs);

            rotation.Timestamps = new List<SubAnimBlock_T>();
            for (int j = 0; j < rotation.numberOfTimestampPairs; j++)
            {
                SubAnimBlock_T element = new SubAnimBlock_T();
                element.nValues = file.ReadInt32();
                element.ofsValues = file.ReadInt32();

                for (int k = 0; k < element.nValues; k++)
                {
                    int var = file.ReadInt32(element.ofsValues + 4 * k);
                    element.timestamp.Add(var);
                }
                rotation.Timestamps.Add(element);
            }
            file.SetReadIndex(indexHolder);

            rotation.numberOfKeyFramePairs = file.ReadInt32();
            rotation.offsetToKeyFramePairs = file.ReadInt32();

            indexHolder = file.GetReadIndex();
            file.SetReadIndex(rotation.offsetToKeyFramePairs);
            for (int j = 0; j < rotation.numberOfKeyFramePairs; j++)
            {
                SubAnimBlock_S substruct = new SubAnimBlock_S();
                substruct.nValues = file.ReadInt32();
                substruct.ofsValues = file.ReadInt32();

                int tempIndex = file.GetReadIndex();
                file.SetReadIndex(substruct.ofsValues);

                for (int k = 0; k < substruct.nValues; k++)
                {
                    ByteVector4 vect = new ByteVector4();
                    vect.x = file.ReadInt16();
                    vect.y = file.ReadInt16();
                    vect.z = file.ReadInt16();
                    vect.w = file.ReadInt16();
                    substruct.anim.Add(vect);
                }
                file.SetReadIndex(tempIndex);
                rotation.Values.Add(substruct);
            }
            file.SetReadIndex(indexHolder);
            bone.Rotation = rotation;


            //Scaling Block
            ABlock_F scale = new ABlock_F();
            scale.Interpolation = file.ReadInt16();
            scale.GlobalSequenceID = file.ReadInt16();
            scale.numberOfTimestampPairs = file.ReadInt32();
            scale.offsetToTimestampPairs = file.ReadInt32();

            indexHolder = file.GetReadIndex();
            file.SetReadIndex(scale.offsetToTimestampPairs);
            //Read TimeStamps
            scale.Timestamps = new List<SubAnimBlock_T>();
            for (int j = 0; j < scale.numberOfTimestampPairs; j++)
            {
                SubAnimBlock_T element = new SubAnimBlock_T();
                element.nValues = file.ReadInt32();
                element.ofsValues = file.ReadInt32();

                for (int k = 0; k < element.nValues; k++)
                {
                    int var = file.ReadInt32(element.ofsValues + 4 * k);
                    element.timestamp.Add(var);
                }
                scale.Timestamps.Add(element);
            }
            file.SetReadIndex(indexHolder); // Continue with the index
            scale.numberOfKeyFramePairs = file.ReadInt32();
            scale.offsetToKeyFramePairs = file.ReadInt32();

            //Read Values
            indexHolder = file.GetReadIndex();
            file.SetReadIndex(scale.offsetToKeyFramePairs);
            scale.Values = new List<SubAnimBlock_F>();
            for (int j = 0; j < scale.numberOfKeyFramePairs; j++)
            {

                SubAnimBlock_F element = new SubAnimBlock_F();
                element.nValues = file.ReadInt32();
                element.ofsValues = file.ReadInt32();

                int tempIndex = file.GetReadIndex();
                file.SetReadIndex(element.ofsValues);

                for (int k = 0; k < element.nValues; k++)
                {
                    WVector3 vector = new WVector3();
                    vector.x = file.ReadFloat();
                    vector.y = file.ReadFloat();
                    vector.z = file.ReadFloat();
                    element.substruct.Add(vector);

                }
                file.SetReadIndex(tempIndex);
                scale.Values.Add(element);
            }
            file.SetReadIndex(indexHolder); // Continue with the next block
            bone.Scaling = scale;           // Assign the block to the newly created translation structure
            WVector3 position = new WVector3();
            position.x = file.ReadFloat();
            position.y = file.ReadFloat();
            position.z = file.ReadFloat();

            bone.PivotPoint = position;


            bones.Add(bone);

            //Particles


        }

    }

    public List<char> result;
    public char[] MAGIC;
    public int IVersion;
    public int IName;
    public int ofsName;
    public E_GLOBALFLAGS type;
    public int nGlobalSequences;
    public int ofsGlovalSequences;
    public int nAnimations;
    public int ofsAnimations;
    public int nAnimationLookup;
    public int ofsAnimationLookup;
    public int nBones;
    public int ofsBones;
    public int nKeyBoneLookup;
    public int ofsKeyBoneLookup;
    public int nVertices;
    public int ofsVertices;
    public int nViews;
    public int nColors;
    public int ofsColors;
    public int nTextures;
    public int ofsTextures;
    public int nTransparency;
    public int ofsTransparency;
    public int nTextureanimations;
    public int ofsTextureanimations;
    public int nTexReplace;
    public int ofsTexReplace;
    public int nRenderFlags;
    public int ofsRenderFlags;
    public int nBoneLookupTable;
    public int ofsBoneLookupTable;
    public int nTexLookup;
    public int ofsTexLookup;
    public int nTexUnits;
    public int ofsTexUnits;
    public int nTransLookup;
    public int ofsTransLookup;
    public int nTexAnimLookup;
    public int ofsTexAnimLookup;
    TheFloats theFloats;
    public int nBoundingTriangles;
    public List<Bone> bones;
    
    int ofsBoundingTriangles;
    int nBoundingVertices;
    int ofsBoundingVertices;
    int nBoundingNormals;
    int ofsBoundingNormals;
    int nAttachments;
    int ofsAttachments;
    int nAttachLookup;
    int ofsAttachLookup;
    int nEvents;
    int ofsEvents;
    int nLights;
    int ofsLights;
    int nCameras;
    int ofsCameras;
    int nCameraLookup;
    int ofsCameraLookup;
    int nRibbonEmitters;
    int ofsRibbonEmitters;
    int nParticleEmitters;
    int ofsParticleEmitters;
    public string ModelName;
    public List<Vertex> vertices;
};
class M2Skin
{
    public M2Skin(string filename)
    {
        WoWFile file = new WoWFile(filename);
        MAGIC = new char[4];
        TRITriangles = new List<WVector3i>();
        skinHeader = new SkinHeader();
        Indices = new List<int>();
        Submeshes = new List<Submesh>();
        MAGIC[0] = file.ReadChar();
        MAGIC[1] = file.ReadChar();
        MAGIC[2] = file.ReadChar();
        MAGIC[3] = file.ReadChar();


        if (!MAGIC.Equals("SKIN")) // valid format for SKIN
        {
            // Initialize the header struct
            skinHeader.nIndices = file.ReadInt32();
            skinHeader.ofsIndices = file.ReadInt32();
            skinHeader.nTriangles = file.ReadInt32();
            skinHeader.ofsTriangles = file.ReadInt32();
            skinHeader.nProperties = file.ReadInt32();
            skinHeader.ofsProperties = file.ReadInt32();
            skinHeader.nSubmeshes = file.ReadInt32();
            skinHeader.ofsSubmeshes = file.ReadInt32();
            skinHeader.nTextureUnits = file.ReadInt32();
            skinHeader.ofsTextureUnits = file.ReadInt32();
            skinHeader.LOD = file.ReadInt32();

            file.SetReadIndex(skinHeader.ofsTriangles);
            for (int i = 0; i < skinHeader.nTriangles / 3; i++)
            {
                int x = file.ReadInt16();
                int y = file.ReadInt16();
                int z = file.ReadInt16();

                TRITriangles.Add(new WVector3i(x, y, z));
            }

            //Skin indices
            file.SetReadIndex(skinHeader.ofsIndices);
            for (int i = 0; i < skinHeader.nIndices; i++)
            {
                int val = file.ReadInt16();
                Indices.Add(val);
            }

            file.SetReadIndex(skinHeader.ofsSubmeshes);
            for (int i = 0; i < skinHeader.nSubmeshes; i++)
            {
                Submesh mesh = new Submesh();

                mesh.ID = file.ReadInt32();
                mesh.StartVertex = file.ReadInt16();
                mesh.nVertices = file.ReadInt16();
                mesh.StartTriangle = file.ReadInt16();
                mesh.nTriangles = file.ReadInt16();
                mesh.nBones = file.ReadInt16();
                mesh.StartBones = file.ReadInt16();
                mesh.Unknown = file.ReadInt16();
                mesh.RootBone = file.ReadInt16();

                WVector3 pos = new WVector3();

                pos.x = file.ReadFloat();
                pos.y = file.ReadFloat();
                pos.z = file.ReadFloat();

                mesh.Position = pos;

                WVector4 floats = new WVector4();
                floats.x = file.ReadFloat();
                floats.y = file.ReadFloat();
                floats.z = file.ReadFloat();
                floats.w = file.ReadFloat();

                mesh.Floats = floats;

                Submeshes.Add(mesh);
            }
        }

    }

    public List<Submesh> Submeshes;
    public char[] MAGIC;
    public SkinHeader skinHeader;
    public List<WVector3i> TRITriangles;
    public List<int> Indices;
};
